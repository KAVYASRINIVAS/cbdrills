const fs=require('fs');

const fileCreation=require('../problem1.js');

fileCreation.createFiles(10,(index)=>{
    fs.open(`./JSON_Files/file${index}.json`,'w',function(err){
        if(err){
            console.log(err);
        }else{
            console.log(`file${index}.json created!`);
            console.log();

            console.log(`Deleting file${index}.json`);
            console.log();
            fs.unlink(`./JSON_Files/file${index}.json`,function(err){
                if(err){
                    console.log(err);
                }else{
                    console.log(`file${index}.json deleted!`);
                    console.log();
                }
            })
        }
    })

});