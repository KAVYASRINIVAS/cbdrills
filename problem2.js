const fs = require('fs');

function cbProblem(pathForDataFile, ucaseFilename, lcaseFilename, splitSentencesFilename, sortFilename, filenamesContainer) {
    fileRead(pathForDataFile, (err, data) => {
        if (err) {
            console.log(err);
        } else {
            let ucaseData = convertToUppercase(data);
            fileWrite(ucaseFilename, ucaseData)
            fileWrite(filenamesContainer, `${ucaseFilename}\n`)
            fileRead(ucaseFilename, function (err, data) {
                if (err) {
                    console.log(err);
                } else {
                    let lcaseData = data.toString().toLowerCase();
                    fileWrite(lcaseFilename, lcaseData);
                    appendData(filenamesContainer, `${lcaseFilename}\n`);
                    fileRead(lcaseFilename, function (err, data) {
                        if (err) {
                            console.log(err);
                        } else {
                            let sentence = [];
                            sentence = splitIntoSentences(data);
                            for (let i = 0; i < sentence.length; i++) {
                                appendData(splitSentencesFilename, `${sentence[i]}\n`);
                            }
                            appendData(filenamesContainer, `${splitSentencesFilename}\n`);
                            fileRead(splitSentencesFilename, function (err, data) {
                                if (err) {
                                    console.log(err);
                                } else {
                                    let content = data.split('\n');
                                    let newfileData = content.reduce((res, line) => {
                                        if (line !== '') {
                                            res.push(line);
                                        }
                                        return res
                                    }, []);
                                    newfileData.sort();
                                    for (let i = 0; i < newfileData.length; i++) {
                                        appendData(sortFilename, `${newfileData[i]}\n`);
                                    }
                                    appendData(filenamesContainer, `${sortFilename}`);
                                    fileRead(filenamesContainer, function (err, data) {
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            deleteFile(data);
                                        }
                                    })
                                }
                            })

                        }
                    })

                }
            })
        }

    })
}

function fileRead(filename, cb) {
    fs.readFile(filename, 'utf-8', function (err, data) {
        if (err) {
            return cb(err, null);
        } else {
            return cb(null, data);
        }
    })
}

function convertToUppercase(data) {
    let upperCaseData = data.toString().toUpperCase();
    return upperCaseData;
}

function fileWrite(filename, data) {
    fs.writeFile(filename, data, function (err) {
        console.log(err);
    })
}

function appendData(filename, data) {
    fs.appendFile(filename, data, function (err) {
        if (err) {
            console.log(err);
        }
    })
}

function splitIntoSentences(data) {
    let newData = data.toString().split(/[!.\n]/);
    let sentence = [];
    newData = newData.filter(sentence => sentence);
    for (let i = 0; i < newData.length; i++) {
        if (newData[i] === ' ') {
            continue;
        }
        let str = newData[i].trim();
        sentence.push(str.charAt(0).toUpperCase() + str.substring(1) + '.');
    }
    return sentence;
}

function deleteFile(data) {
    let filename = data.toString().split('\n');
    console.log(filename);
    for (let i = 0; i < filename.length; i++) {
        fs.unlink(filename[i], function (err) {
            if (err) {
                console.log(err);
            }
        })
        console.log(`Deleted file: ${filename[i]}`);
    }
}

module.exports = { cbProblem };