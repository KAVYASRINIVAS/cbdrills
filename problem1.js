const fs=require('fs');

function createFiles(n, callback) {
    fs.mkdir('./JSON_Files', function (err) {
        if (err) {
            console.log(err);
        } else {
            console.log('New Directory created');
            console.log();
            for(let i=1;i<=n;i++){
                console.log(`Creating file${i}.json`);
                console.log();
                callback(i);
            }
        }
    }
    )
}

module.exports={createFiles};